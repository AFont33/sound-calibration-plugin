#!/usr/bin/python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
import re

version = ''
with open('sound_calibration_plugin/__init__.py', 'r') as fd:
    version = re.search(r'^__version__\s*=\s*[\'"]([^\'"]*)[\'"]',
                        fd.read(), re.MULTILINE).group(1)

if not version:
    raise RuntimeError('Cannot find version information')

requirements = [
    'numpy',
    'scipy',
    'sounddevice',
]

setup(
    name='sound-calibration-plugin',
    version=version,
    description="""Sound system calibration""",
    author='Rachid Azizi',
    author_email='azi92rach@gmail.com',
    license='Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>',
    url='https://bitbucket.org/azi92rach/sound-calibration-plugin',

    include_package_data=True,
    packages=find_packages(exclude=['contrib', 'docs', 'tests', 'examples', 'deploy', 'reports']),
    install_requires=requirements,
    package_data={'sound_calibration_plugin': [
            'resources/*.*',
        ]
    },
)
