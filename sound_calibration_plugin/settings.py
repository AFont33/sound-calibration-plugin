#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
from pyforms import conf

SOUND_PLUGIN_ICON = os.path.join( os.path.dirname(__file__), 'resources', 'speaker.png' )

SOUND_PLUGIN_WINDOW_SIZE = 800, 700
SOUND_HISTORY_BROAD_PLUGIN_WINDOW_SIZE = 700, 600
SOUND_HISTORY_PURE_PLUGIN_WINDOW_SIZE = 700, 600
SOUND_HISTORY_TREND_PLUGIN_WINDOW_SIZE = 700, 600

SOUND_WITHOUT_DAQ = False # it will be automatically change in sound_utils.py if you don't have the sound card

SOUND_PLUGIN_REFRESH_RATE   = 2000

SECONDS_SOUND = 10  # Length of the sound vector

# PATH
# Default values of the plugin
SOUND_PLUGIN_PATH_DEFAULT = conf.GENERIC_EDITOR_PLUGINS_PATH + '/sound-calibration-plugin/default_values.npy'
# Data folder where to save all the data
SOUND_PLUGIN_PATH_DATA = conf.GENERIC_EDITOR_PLUGINS_PATH + '/sound-calibration-plugin/DATA'
if not os.path.exists(SOUND_PLUGIN_PATH_DATA): # if DATA does not exist, create it
    os.makedirs(SOUND_PLUGIN_PATH_DATA)
# Name files
SOUND_PLUGIN_PURETONE_PATH  = SOUND_PLUGIN_PATH_DATA + '/puretone_hystory.npy'
SOUND_PLUGIN_BROADBAND_PATH = SOUND_PLUGIN_PATH_DATA + '/broadbandnoise_hystory.npy'
SOUND_PLUGIN_TRENDAMP_PATH  = SOUND_PLUGIN_PATH_DATA + '/TREND_AMP'
SOUND_PLUGIN_TRENDFREQ_PATH = SOUND_PLUGIN_PATH_DATA + '/TREND_FREQ'

# Color graphs
SOUND_PLUGIN_COLOR_HISTORY = 'ko-'
SOUND_PLUGIN_COLOR_TASK    = 'bo-'
SOUND_PLUGIN_COLOR_DEFAULT    = 'red'