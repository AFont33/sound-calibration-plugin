# !/usr/bin/python3
# -*- coding: utf-8 -*-

import sound_calibration_plugin.settings as setting
import os
import numpy as np
from time import localtime, strftime

class SoundDataUtils:
    """ This class is organized to save and read all the the variables into/from a file .mat for the sound plugin."""

    def __init__(self):
        if not os.path.exists(setting.SOUND_PLUGIN_TRENDAMP_PATH):  # if TREND_AMP folde does not exist, create it
            os.makedirs(setting.SOUND_PLUGIN_TRENDAMP_PATH)
        if not os.path.exists(setting.SOUND_PLUGIN_TRENDFREQ_PATH):  # if TREND_FREQ folder does not exist, create it
            os.makedirs(setting.SOUND_PLUGIN_TRENDFREQ_PATH)

        # Pure tone data
        if not os.path.exists(setting.SOUND_PLUGIN_PURETONE_PATH):
            # If there not exist the file, create new variables
            self.pure_date = np.array([])
            self.pure_freq = np.array([])
            self.pure_loop = np.array([])
            self.pure_amp  = np.array([])
            self.pure_db   = np.array([])
            self.pure_side = np.array([])
            self.pure_soundcard = np.array([])

        else:
            # Otherwise, read the variables from the file saved last time.
            file = np.load(setting.SOUND_PLUGIN_PURETONE_PATH)
            file = file.item()
            self.pure_date = file['Pdate']
            self.pure_freq = file['PFreq']
            self.pure_loop = file['PLoop']
            self.pure_amp  = file['PAmp']
            self.pure_db   = file['PdB']
            self.pure_side = file['Pside']
            self.pure_soundcard = file['Psoundcard']

        # Broadband data
        if not os.path.exists(setting.SOUND_PLUGIN_BROADBAND_PATH):
            # If there not exist the file, create new variables
            self.broad_date    = np.array([])
            self.broad_minFreq = np.array([])
            self.broad_maxFreq = np.array([])
            self.broad_loop    = np.array([])
            self.broad_amp     = np.array([])
            self.broad_db      = np.array([])
            self.broad_side    = np.array([])
            self.broad_soundcard = np.array([])
        else:
            # Otherwise, read the variables from the file saved last time.
            file = np.load(setting.SOUND_PLUGIN_BROADBAND_PATH)
            file = file.item()
            self.broad_date    = file['Bdate']
            self.broad_minFreq = file['BFreq_min']
            self.broad_maxFreq = file['BFreq_max']
            self.broad_loop    = file['BLoop']
            self.broad_amp     = file['BAmp']
            self.broad_db      = file['BdB']
            self.broad_side    = file['Bside']
            self.broad_soundcard = file['Bsoundcard']

    # ------------------ PURE TONE DATA --------------------------------
    def savePuretoneResult(self, freq, loop, amp, db, side, soundcard):
        """ Save the variables after run a Pure tone calibration. """
        self.pure_date = np.append(self.pure_date, [self.__getTime()])
        self.pure_freq = np.append(self.pure_freq, [freq])
        self.pure_loop = np.append(self.pure_loop, [loop])
        self.pure_amp = np.append(self.pure_amp, [amp])
        self.pure_db = np.append(self.pure_db, [db])
        self.pure_side = np.append(self.pure_side, [side])
        self.pure_soundcard = np.append(self.pure_soundcard, [soundcard])

        # save data
        self.__savePuretone()

    def __savePuretone(self):
        """ Save variables in a file."""
        np.save(setting.SOUND_PLUGIN_PURETONE_PATH, {'Pdate':self.pure_date, 'PFreq': self.pure_freq,
                                                     'PLoop': self.pure_loop, 'PAmp': self.pure_amp,
                                                     'PdB': self.pure_db, 'Pside':self.pure_side,
                                                     'Psoundcard':self.pure_soundcard})

    def getPuretoneResult(self):
        """ Return the variables of the last pure tone calibration. """
        return [self.pure_date, self.pure_freq, self.pure_loop, self.pure_amp,  self.pure_db, self.pure_side, self.pure_soundcard]

    def removePuretoneResult(self,idx):
        try:
            self.pure_date = np.delete(self.pure_date, idx)
            self.pure_freq = np.delete(self.pure_freq,idx)
            self.pure_loop = np.delete(self.pure_loop, idx)
            self.pure_amp = np.delete(self.pure_amp, idx)
            self.pure_db = np.delete(self.pure_db, idx)
            self.pure_side = np.delete(self.pure_side, idx)
            self.pure_soundcard = np.delete(self.pure_soundcard, idx)
            # save data
            self.__savePuretone()
            return None

        except Exception as err:
            return err

    # ---------------- BROAD BAND NOISE DATA --------------------------------
    def saveBroadbandResult(self, minfreq, maxfreq, loop, amp, db, side, soundcard):
        """ Save the variables after run a braodband noise calibration. """
        self.broad_date = np.append(self.broad_date, [self.__getTime()])
        self.broad_minFreq = np.append(self.broad_minFreq, [minfreq])
        self.broad_maxFreq = np.append(self.broad_maxFreq, [maxfreq])
        self.broad_loop = np.append(self.broad_loop, [loop])
        self.broad_amp = np.append(self.broad_amp, [amp])
        self.broad_db = np.append(self.broad_db, [db])
        self.broad_side = np.append(self.broad_side, [side])
        self.broad_soundcard = np.append(self.broad_soundcard, [soundcard])

        # save data
        self.__saveBroadband()

    def __saveBroadband(self):
        """ Save variables in a file."""
        np.save(setting.SOUND_PLUGIN_BROADBAND_PATH, {'Bdate':self.broad_date, 'BFreq_min': self.broad_minFreq,
                            'BFreq_max': self.broad_maxFreq, 'BLoop': self.broad_loop,
                           'BAmp': self.broad_amp, 'BdB': self.broad_db, 'Bside':self.broad_side,
                                                      'Bsoundcard':self.broad_soundcard})

    def getBroadbandResult(self):
        """ Return the variables of the last broadband noise calibration. """
        return [self.broad_date, self.broad_minFreq, self.broad_maxFreq, self.broad_loop, self.broad_amp,
                self.broad_db, self.broad_side, self.broad_soundcard]

    def removeBroadbandResult(self, idx):
        try:
            self.broad_date = np.delete(self.broad_date, idx)
            self.broad_minFreq = np.delete(self.broad_minFreq,idx)
            self.broad_maxFreq = np.delete(self.broad_maxFreq, idx)
            self.broad_loop = np.delete(self.broad_loop, idx)
            self.broad_amp = np.delete(self.broad_amp, idx)
            self.broad_db = np.delete(self.broad_db, idx)
            self.broad_side = np.delete(self.broad_side, idx)
            self.broad_soundcard = np.delete(self.broad_soundcard, idx)
            # save data
            self.__saveBroadband()
            return None

        except Exception as err:
            return err

    # ---------------- TREND DATA AMP ------------------
    def saveTrendAmp(self, fixFreq, amp, db, side, soundcard):
        """ Save the trend amlification. """
        np.save(setting.SOUND_PLUGIN_TRENDAMP_PATH + '/amp_' + self.__getTime() + '_' + side,
                    {'FixFreq': fixFreq, 'Amp': amp, 'amp2dB': db, 'soundcard': soundcard})

    def getLastAmp(self):
        """ Read the last trend amplification. Return the vector data and the name of the file. """
        path = setting.SOUND_PLUGIN_TRENDAMP_PATH
        mtime = lambda f: os.stat(os.path.join(path, f)).st_mtime
        nameFiles = sorted(os.listdir(path), key=mtime, reverse=True)
        nameFiles = [f for f in nameFiles if f.endswith('.npy')]
        if nameFiles == []:
            raise FileExistsError('The folder is empty.')
        else:
            nameLastFile = setting.SOUND_PLUGIN_TRENDAMP_PATH + '/' + nameFiles[0]
            file = np.load(nameLastFile)
            file = file.item()
            return [file['FixFreq'], file['Amp'],file['amp2dB'], file['soundcard']], nameLastFile

    def getAmp(self, nameFile):
        """ Read the trend amplification given the name of the file. Return the vector data."""
        file = np.load(nameFile)
        file = file.item()
        return [file['FixFreq'], file['Amp'], file['amp2dB'], file['soundcard']]

    # ---------------- TREND DATA FREQ ------------------
    def saveTrendFreq(self, fixAmp, freq, db, side, soundcard):
        """ Save the trend frequency. """
        np.save(setting.SOUND_PLUGIN_TRENDFREQ_PATH + '/freq_' + self.__getTime() + '_' + side,
                    {'FixAmp': fixAmp, 'Freq': freq, 'freq2dB': db, 'soundcard': soundcard})

    def getLastFreq(self):
        """ Read the last trend frequency. Return the vector data and the name of the file. """
        path = setting.SOUND_PLUGIN_TRENDFREQ_PATH
        mtime = lambda f: os.stat(os.path.join(path, f)).st_mtime
        nameFiles = sorted(os.listdir(path), key=mtime, reverse=True)
        nameFiles = [f for f in nameFiles if f.endswith('.npy')]
        if nameFiles == []:
            raise FileExistsError('The folder is empty.')
        else:
            nameLastFile = setting.SOUND_PLUGIN_TRENDFREQ_PATH + '/' + nameFiles[0]
            file = np.load(nameLastFile)
            file = file.item()
            return [file['FixAmp'], file['Freq'],file['freq2dB'], file['soundcard']], nameLastFile

    def getFreq(self, nameFile):
        """ Read the trend frequency given the name of the file. Return the vector data."""
        file = np.load(nameFile)
        file = file.item()
        return [file['FixAmp'], file['Freq'], file['freq2dB'], file['soundcard']]

    def __getTime(self):
        """ Tool used into the name of the files to preserve the time when you saved it."""
        return strftime("%d%b%Y_%H%M%S", localtime())