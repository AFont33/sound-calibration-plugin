# !/usr/bin/python3
# -*- coding: utf-8 -*-


import numpy as np
from scipy.fftpack import fft
from scipy.signal import firwin, lfilter
import sound_calibration_plugin.settings as setting
from mccdaqR import DaqR_usb1608G

""" Tools to create the sounds, compute the FFT and more. """

# Air pressure reference
PRESSURE_REF = 20 * 1e-6

# ---------------------------------- DAQ --------------------------------------------
daq = []
try: # Try to open the DAQ otherwise you will use a simulation
    daq = DaqR_usb1608G()
    setting.SOUND_WITHOUT_DAQ = False
except Exception:
    print('No DAQ installed! Please, plug in the DAQ, close and open the GUI.')
    setting.SOUND_WITHOUT_DAQ = True

def scanSignal(channel, seconds, FsIn, sensitivity):
    c = channel  # channel of DAQ
    gain = 10  # in Volt. 10V, 5V, 2V, 1V
    mode = 0  # 0 for single endian, 1 for differential
    t = seconds  # seconds
    sens = 1000 / sensitivity  # microphone sensitivity, useful to convert V to Pa

    # Scan the channel
    vecOut, vecTime = daq.scan(channel=c, g=gain, m=mode, timeScan=t, frequency=FsIn)

    return vecOut * sens, vecTime

# ---------------------------------- SOUND --------------------------------------------

def genPureTone(duration, FsOut, fs, amplitude):
    """ Generation of a pure tone. """
    tvec = np.linspace(0, duration, duration * FsOut)
    s1 = amplitude * np.sin(2 * np.pi * fs * tvec)  # Sound vector
    s2 = np.zeros(s1.size)                          # Empty sound
    return s1, s2

def genBreadbandNoise(duration, FsOut, band_fs, amplitude):
    """ Generation of a broadband noise using Gaussian noise. """
    # Generation of white noise.
    mean = 0
    std = 1
    white_noise = amplitude * np.random.normal(mean, std, size=FsOut * (duration + 1)) # 1 second more that will be removed at the end

    # Filter the white noise using a pass-band
    Fn = 10000 # Lenght of the filter
    band_pass = firwin(Fn, [band_fs[0]/(FsOut*0.5), band_fs[1]/(FsOut*0.5)], pass_zero=False) # Pass-band fileter
    band_noise = lfilter(band_pass, 1, white_noise) # Filtering
    s1 = band_noise[FsOut:FsOut*(duration+1)] # remove the first second: it has some zeros casused by the phase of the filter
    s2 = np.zeros(s1.size) # Empty vector of the same size.
    return s1, s2

def droadband_db(vecsOut, band, FsIn, gain_michrophone = 0):
    """ dB calculation for broadband noise. """
    s = len(vecsOut[0]) // FsIn  # Seconds of signal
    meanFFT = np.zeros(FsIn * s // 2 + 1) # variable useful when we will have to compute the mean of the FFT
    N = len(vecsOut)
    # Compute the FFT for each signal recorded
    for i in range(N):
        F, T = comp_fft(vecsOut[i], FsIn)  # fft
        meanFFT = meanFFT + F
    # Compute the mean of FFT
    meanFFT = meanFFT / N
    # Take care only the frequencies between the band.
    result_db = 10 * np.log10(sum(meanFFT[band[0] * s//2 : band[1] * s * 2 + 1]) / PRESSURE_REF ** 2) - gain_michrophone
    return result_db

def pureTone_db(vecsOut, freq, FsIn, gain_michrophone = 0):
    """ dB calculation for pure tone. """
    s = len(vecsOut[0])//FsIn # seconds of signal
    meanFFT = np.zeros(FsIn*s//2 + 1) # variable useful when we will have to compute the mean of the FFT
    N = len(vecsOut)
    for i in range(N):
        F, T = comp_fft(vecsOut[i], FsIn) #fft
        meanFFT = meanFFT + F
    meanFFT = meanFFT/N
    # Take care only the octave of the specific frequency.
    result_db = 10*np.log10(max(meanFFT[freq*s//2:freq*s*2+1]) / PRESSURE_REF ** 2) - gain_michrophone
    return result_db

def comp_fft(signal, Fs):
    """ Compute the FFT module of signal """
    L = len(signal)
    s = len(signal) // Fs # seconds of the signal
    T = np.arange(0, Fs//2+Fs/L, Fs/L) # vector of frequencies

    xdft = fft(signal) # Entire fft
    xdft = xdft[0:L//2+1] # Only positive side
    lin_space = (1/(Fs*L*s))*np.abs(xdft)**2 # Normalization
    lin_space[1: len(lin_space) - 1] = 2 *  lin_space[1: len(lin_space) - 1] # Vector of frequencies.
    return lin_space, T
